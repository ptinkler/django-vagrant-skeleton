Welcome to {{ project_name }}'s documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

   install
   deploy
   tests



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
