========================
django-vagrant-skeleton
========================

A project template for Django 1.6 that provides a project structure based on the
Two Scoops of Django best practice guide (https://github.com/twoscoops/django-twoscoops-project).
The template also provides a basic Vagrant config file and provisions for setting up an VirtualBox-based
virtual machine for developing on.

To use this project template effectively you will need to:

#. Install Vagrant (http://www.vagrantup.com/).
#. Create a virtual environment using virtualenv (http://www.virtualenv.org/en/latest/).
#. Install Django 1.6+ in the virtualenv.
#. Create your project directory.
#. Run::
    $ django-admin.py startproject --extension=py,rst,html --template=https://github.com/ptink/django-vagrant-skeleton/archive/master.zip  <your_project_name> <your_project_dir>
#. Run::
    $ vagrant up --provision

